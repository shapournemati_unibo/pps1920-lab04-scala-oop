package u04lab.code

import Optionals._
import Lists._
import Streams._
import u04lab.code.Lists.List.Cons

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]

  def allSoFar(): List[A]

  def reversed(): PowerIterator[A]
}

case class PowerIteratorImpl[A](var stream: Stream[A]) extends PowerIterator[A] {

  private var past: List[A] = List.Nil()

  override def next(): Option[A] = {
    stream match {
      case Stream.Cons(h, t) => {
        past = List.append(past, List.Cons(h(), List.Nil()))
        stream = t()
        Option.of(h())
      }
      case _ => Option.empty
    }
  }

  override def allSoFar(): List[A] = past

  override def reversed(): PowerIterator[A] = PowerIteratorImpl(List.toStream(reversedPast))

  private def reversedPast = List.reverse(past)
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]

  def fromList[A](list: List[A])

  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIteratorImpl(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): Unit = PowerIteratorImpl(List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIteratorImpl(randomBooleansStream(size))

  private def randomBooleansStream(size: Int): Stream[Boolean] = Stream.generate(Random.nextBoolean())

}
